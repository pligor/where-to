package com.pligor.whereto;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;

public class GPSReceiver implements LocationListener {
	private Context context;
	private LocationManager locationManager;

	private final long minInterval_msecs = 1000;
	private final float minDistance_meters = 5;

	private double currentLatitude = 0;
	private double currentLongtitude = 0;
	private float currentAccuracy = 0; // in meters

	public GPSReceiver(Context context) {
		this.context = context;
	}

	/**
	 * In current implementation the best provider is only chosen ONCE at the beginning
	 * 
	 * @return
	 */
	public LocationManager startLocationManager() {

		HashMap<String, Boolean> provider_enabled = new HashMap<String, Boolean>();

		if(this.locationManager == null) {
			this.locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);

			provider_enabled.put(LocationManager.GPS_PROVIDER, this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
			provider_enabled.put(LocationManager.NETWORK_PROVIDER, this.locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
		}

		String best_provider;

		// case with network needs more attention
		if(provider_enabled.get(LocationManager.NETWORK_PROVIDER)) { // network
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_COARSE); // coarse has a value of 2 which is like the medium accuracy
			criteria.setAltitudeRequired(false);
			criteria.setPowerRequirement(Criteria.NO_REQUIREMENT); // I guess how much battery to spend

			best_provider = this.locationManager.getBestProvider(criteria, false); // TODO check this if false is most appropriate
		}
		else if(provider_enabled.get(LocationManager.GPS_PROVIDER)) { // gps
			best_provider = LocationManager.GPS_PROVIDER;
		}
		else {
			Toast.makeText(this.context, "No other network is found. Please open your GPS", Toast.LENGTH_LONG).show();

			best_provider = LocationManager.NETWORK_PROVIDER;
		}
		Log.i("pl", "best provider found: " + best_provider);

		locationManager.requestLocationUpdates(best_provider, this.minInterval_msecs, this.minDistance_meters, this);

		return this.locationManager;
	}

	public void stopLocationManager() {
		if(this.locationManager != null) {
			this.locationManager.removeUpdates(this);
			this.locationManager = null;
		}
	}

	/**
	 * //TODO delete this method when more proficient Telika auto mou epistrefei tria strings: "network" "passive" "gps"
	 * 
	 * @return
	 */
	public void listAllProviders() {
		if(this.locationManager == null) {
			return;
		}

		int count = 1;
		List<String> providers = this.locationManager.getAllProviders();
		for (String provider : providers) {
			Log.i("pl", "provider" + (count++) + " : " + provider);
		}
	}

	public static int convDegreesToInt(double degrees) {
		return (int) (degrees * 1E6);
	}

	public static double convDegreesToDouble(int degrees) {
		return (double) degrees / (double) 1E6;
	}

	// read only
	public int getCurrentLatitude() {
		// return (int) (this.currentLatitude * 1E6);
		return GPSReceiver.convDegreesToInt(this.currentLatitude);

	}

	public int getCurrentLongtitude() {
		// return (int) (this.currentLongtitude * 1E6);
		return GPSReceiver.convDegreesToInt(this.currentLongtitude);
	}

	public double getCurrentPureLatitude() {
		return this.currentLatitude;
	}

	public double getCurrentPureLongtitude() {
		return this.currentLongtitude;
	}

	public float getCurrentAccuracy() {
		return this.currentAccuracy;
	}

	public void onLocationChanged(Location loc) {
		this.currentLatitude = loc.getLatitude();
		this.currentLongtitude = loc.getLongitude();
		this.currentAccuracy = loc.getAccuracy();

		Log.i("pl", "New loc: " + this.currentLatitude + " ; " + this.currentLongtitude + " with accuracy: " + this.currentAccuracy);
	}

	public void onProviderDisabled(String provider) {
		// Toast.makeText(context, "Please enable your GPS receiver for better accuracy", Toast.LENGTH_LONG).show();
		Log.i("pl", "provider: " + provider + " is disabled");
	}

	public void onProviderEnabled(String provider) {
		Log.i("pl", "provider: " + provider + " is enabled");
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {

		// extras.get("satellites");

		String statusString = null;

		if(status == LocationProvider.AVAILABLE) {
			statusString = "AVAILABLE";
		}
		else if(status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
			statusString = "TEMPORARILY_UNAVAILABLE";
		}
		else if(status == LocationProvider.OUT_OF_SERVICE) {
			statusString = "OUT_OF_SERVICE";
		}

		Log.i("pl", "provider: " + provider + " has a new status: " + statusString);
	}

	public static GeoPoint getGeoPointFromLatAndLon(double lat, double lon) {
		return new GeoPoint(GPSReceiver.convDegreesToInt(lat), GPSReceiver.convDegreesToInt(lon));
	}
}
