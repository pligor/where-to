package com.pligor.whereto;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends Activity {

	private static final int DELAY_MSEC = 2000;

	private SharedPreferences settings;

	ImageView splashPinLeft;
	ImageView splashPinRight;
	TextView parkingsCounter;
	TextView gasStationsCounter;

	private static final int originalPinWidth = 576;
	private static final int originalPinHeight = 600;

	private static final int maxItems = 990;

	private DatabaseHandler dbHandler;

	private static final String INITIATED = "initiated";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.initViews();

		this.dbHandler = new DatabaseHandler(this);

		this.settings = this.getSharedPreferences("settings", Application.MODE_PRIVATE);
		if(!this.settings.getBoolean("initiated", false)) {
			this.resetDB();
			this.initDB();
			Log.i("pl", "first launch");
		}

		this.repositionPins();

		Thread timer = new Thread() {

			@Override
			public void run() {
				super.run();

				try {
					Thread.sleep(SplashActivity.DELAY_MSEC);
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				finally {
					Editor editor = SplashActivity.this.settings.edit();
					editor.putBoolean(SplashActivity.INITIATED, true);
					editor.commit();

					Intent intent = new Intent(SplashActivity.this, MainActivity.class);
					SplashActivity.this.startActivity(intent);
					SplashActivity.this.finish();
				}
			}
		};

		timer.start();
	}

	private void repositionPins() {
		int itemCount;
		String text;

		itemCount = this.dbHandler.countLocationsByCategoryId(1); // parking
		text = itemCount + " parkings";
		this.parkingsCounter.setText(text);
		this.setPinDimensions(itemCount, this.splashPinLeft.getLayoutParams());
		// Log.i("pl", text);

		itemCount = this.dbHandler.countLocationsByCategoryId(2); // gas station
		text = itemCount + " gas stations";
		this.gasStationsCounter.setText(text);
		this.setPinDimensions(itemCount, this.splashPinRight.getLayoutParams());
		// Log.i("pl", text);
	}

	private void setPinDimensions(int item_count, LayoutParams layoutParams) {
		if(item_count > SplashActivity.maxItems) { // threshold
			item_count = SplashActivity.maxItems;
		}

		int widthDip = (int) (Math.log10(10 + item_count) * 80); // 80 does not have any special meaning. just looks nice
		int heightDip = widthDip * Math.round(originalPinHeight / originalPinWidth);
		Resources resources = this.getResources();
		int widthPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, widthDip, resources.getDisplayMetrics());
		int heightPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, heightDip, resources.getDisplayMetrics());

		layoutParams.width = widthPx;
		layoutParams.height = heightPx;
		// Log.i("pl", widthPx + " width pixels");
		// Log.i("pl", heightPx + " height pixels");
	}

	private void initViews() {
		this.setContentView(R.layout.activity_splash);

		this.splashPinLeft = (ImageView) this.findViewById(R.id.splashPinLeft);
		this.splashPinRight = (ImageView) this.findViewById(R.id.splashPinRight);
		this.parkingsCounter = (TextView) this.findViewById(R.id.parkingsCounter);
		this.gasStationsCounter = (TextView) this.findViewById(R.id.gasStationsCounter);
	}

	private void resetDB() {
		Boolean deleted = this.deleteDatabase(DatabaseHandler.DATABASE_NAME);
		// if(deleted) { this.initDB(); }
	}

	private void initDB() {
		// Log.i("pl", "db len: " + this.databaseList().length);
		// for (String name : this.databaseList()) {
		// Log.i("pl", "name is: " + name);
		// }
		Log.i("pl", "db len: " + this.getDatabasePath(DatabaseHandler.DATABASE_NAME));

		this.dbHandler.getReadableDatabase(); // simply to trigger create methods if necessary ;)
		this.dbHandler.populate();
		this.dbHandler.close();
	}
}
