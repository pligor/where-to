package com.pligor.whereto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.android.maps.GeoPoint;

public class DatabaseHandler extends SQLiteOpenHelper {
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Static variables
	private static final int DATABASE_VERSION = 1;

	static final String DATABASE_NAME = "whereto";

	// Category table name
	static final String TABLE_CATEGORY = "category";

	// Category table columns names
	private static final String CATEGORY_ID = "id";
	private static final String CATEGORY_NAME = "name";
	private static final String CATEGORY_ICON = "icon";

	// Category table name
	static final String TABLE_LOCATION = "location";
	// Category table columns names
	static final String LOCATION_ID = "id";
	static final String LOCATION_NAME = "name";
	static final String LOCATION_LAT = "lat";
	static final String LOCATION_LON = "lon";
	static final String LOCATION_CATEGORY_ID = "category_id";

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// IF YOU FUCKUP THE SQL CREATE STATEMENTS YOU ARE GOING TO NEED context.deleteDatabase("DB_NAME");
		// //http://stackoverflow.com/questions/4406067/how-to-delete-sqlite-database-from-android-programmatically
		Log.i("pl", "inside onCreate");

		String CREATE_TABLE_CATEGORY = "CREATE TABLE " + DatabaseHandler.TABLE_CATEGORY + " (" + DatabaseHandler.CATEGORY_ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT," + DatabaseHandler.CATEGORY_NAME + " TEXT NOT NULL," + DatabaseHandler.CATEGORY_ICON + " TEXT" + ")";

		String CREATE_TABLE_LOCATION = "CREATE TABLE " + DatabaseHandler.TABLE_LOCATION + " (" + DatabaseHandler.LOCATION_ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT," + DatabaseHandler.LOCATION_NAME + " TEXT NOT NULL," + DatabaseHandler.LOCATION_LAT
				+ " REAL NOT NULL,\n" + DatabaseHandler.LOCATION_LON + " REAL NOT NULL," + DatabaseHandler.LOCATION_CATEGORY_ID + " INTEGER NOT NULL,"
				+ " FOREIGN KEY(" + DatabaseHandler.LOCATION_CATEGORY_ID + ") REFERENCES " + DatabaseHandler.TABLE_CATEGORY + "(" + DatabaseHandler.CATEGORY_ID
				+ ")" + ")";

		db.execSQL(CREATE_TABLE_CATEGORY);
		db.execSQL(CREATE_TABLE_LOCATION);
	}

	public void populate() {
		Category category;
		Location location;

		category = new Category();
		category.setName("Parking");
		category.setIcon("parking");
		this.insert(category);

		location = new Location(new GeoPoint(38129594, 23756495), "Safe and Sound", "Safe and Sound");
		location.setName("Safe and Sound");
		this.insert(location, category);

		// location = new Location(new GeoPoint(38123366, 23762284), "Plido", "Plido");
		// location.setName("Plido");
		// this.insert(location, category);

		// /////////////////////////////////////////////////////////////////

		category = new Category();
		category.setName("Gas Station");
		category.setIcon("gas_station");
		this.insert(category);

		location = new Location(new GeoPoint(38129831, 23758534), "Gaselinos", "Gaselinos");
		location.setName("Gaselinos");
		this.insert(location, category);

		// location = new Location(new GeoPoint(38121416, 23755678), "Shell", "Shell");
		// location.setName("Shell");
		// this.insert(location, category);
	}

	public boolean import_from_json(String input) {
		JSONArray whereto;
		try {
			whereto = new JSONArray(input);

			for (int i = 0; i < whereto.length(); i++) {
				JSONObject jsonObject = whereto.getJSONObject(i);

				Iterator<String> keys = jsonObject.keys();
				String category_name = keys.next();

				Category category = this.getCategoryByName(category_name); // grab category

				jsonObject = jsonObject.getJSONObject(category_name);

				if(category == null) { // category is not found inside existing categories
					category = new Category();

					category.setName(category_name);
					category.setIcon(jsonObject.getString("icon"));

					this.insert(category);

					Log.i("pl", "the cat " + category_name + " is a new cat");
				}
				else {
					Log.i("pl", "the cat " + category_name + " is an old cat");
				}

				JSONArray locations = jsonObject.getJSONArray("locations");
				// loop for locations
				for (int j = 0; j < locations.length(); j++) {
					JSONObject locationJsonObject = locations.getJSONObject(j);

					Iterator<String> location_keys = locationJsonObject.keys();
					String location_name = location_keys.next();

					Location location = this.getLocationByName(location_name);

					locationJsonObject = locationJsonObject.getJSONObject(location_name);

					if(location == null) { // category is not found inside existing categories
						GeoPoint point = new GeoPoint(GPSReceiver.convDegreesToInt(locationJsonObject.getDouble("lat")),
								GPSReceiver.convDegreesToInt(locationJsonObject.getDouble("lon")));
						location = new Location(point, location_name, location_name);
						location.setName(location_name);

						this.insert(location, category);

						Log.i("pl", "the loc " + location_name + " is a new loc");
					}
					else {
						Log.i("pl", "the loc " + location_name + " is an old loc");
					}
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public String export_to_json() {
		JSONObject locJSON;
		JSONObject objLoc;

		JSONArray locsArray;

		JSONObject catJsonObject;
		JSONObject objCat;

		JSONArray whereto = null;

		List<Category> categories;

		List<Location> locations;

		try {
			categories = this.getAll(new Category());
			whereto = new JSONArray();
			for (Category category : categories) {
				locations = category.getLocations(this);

				locsArray = new JSONArray();

				for (Location location : locations) {
					locJSON = new JSONObject();
					locJSON.put("lat", location.getLat());
					locJSON.put("lon", location.getLon());

					objLoc = new JSONObject();
					objLoc.put(location.getName(), locJSON);

					locsArray.put(objLoc);
				}

				catJsonObject = new JSONObject();
				catJsonObject.put("icon", category.getIcon());
				catJsonObject.put("locations", locsArray);

				objCat = new JSONObject();
				objCat.put(category.getName(), catJsonObject);

				whereto.put(objCat);
			}

			return whereto.toString(4);
		}
		catch (JSONException e) {
			e.printStackTrace();
			Log.i("pl", "some error converting object to string");
		}

		return null;
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS); // drop older table if existed
		// this.onCreate(db); // Create tables again

		// just do nothing for the time being :P
	}

	public void insert(Category category) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(DatabaseHandler.CATEGORY_NAME, category.getName());

		// TODO is this if necessary ?
		// if(category.getIcon() != null) {
		contentValues.put(DatabaseHandler.CATEGORY_ICON, category.getIcon());
		// }

		db.insert(DatabaseHandler.TABLE_CATEGORY, null, contentValues); // Inserting Row

		int auto_inc = this.getAutoIncrement(db);

		db.close(); // Closing database connection

		category.setId(auto_inc);
	}

	public void insert(Location location, Category category) {

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues contentValues = new ContentValues();

		contentValues.put(DatabaseHandler.LOCATION_NAME, location.getName());
		contentValues.put(DatabaseHandler.LOCATION_LAT, location.getLat());
		contentValues.put(DatabaseHandler.LOCATION_LON, location.getLon());

		contentValues.put(DatabaseHandler.LOCATION_CATEGORY_ID, category.getId());

		db.insert(DatabaseHandler.TABLE_LOCATION, null, contentValues); // Inserting Row

		int auto_inc = this.getAutoIncrement(db);

		db.close(); // Closing database connection

		location.setId(auto_inc);
	}

	/**
	 * i THINK it works only with a writeable database
	 * 
	 * @param db
	 * @return
	 */
	public int getAutoIncrement(SQLiteDatabase db) {
		String sql = "SELECT last_insert_rowid()";
		Cursor cursor = db.rawQuery(sql, null);

		if(cursor.moveToFirst()) {
			return Integer.parseInt(cursor.getString(0));
		}

		return 0;
	}

	// TESTED OK
	public List getAll(Object object) {
		List list = new ArrayList();

		SQLiteDatabase db = this.getReadableDatabase(); // this works fine

		String query = "SELECT * FROM ";

		if(object instanceof Category) {
			// Category new_name = (Category) object;
			query += DatabaseHandler.TABLE_CATEGORY;
			Cursor cursor = db.rawQuery(query, null);

			if(cursor.moveToFirst()) {
				do { // looping through all rows and adding to list
					Category category = new Category();
					category.setId(cursor.getInt(0));
					category.setName(cursor.getString(1));
					category.setIcon(cursor.getString(2));

					list.add(category); // Adding contact to list
				}
				while (cursor.moveToNext());
			}
		}
		else if(object instanceof Location) {
			query += DatabaseHandler.TABLE_LOCATION;
			Cursor cursor = db.rawQuery(query, null);

			if(cursor.moveToFirst()) {
				do { // looping through all rows and adding to list
					Location location = new Location(cursor);
					list.add(location); // Adding to list
				}
				while (cursor.moveToNext());
			}
		}

		db.close();

		return list;
	}

	public void delete(Category category) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(DatabaseHandler.TABLE_CATEGORY, DatabaseHandler.CATEGORY_ID + " = ?", new String[] {
			String.valueOf(category.getId())
		});

		db.close();
	}

	public void delete(Location location) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(DatabaseHandler.TABLE_LOCATION, DatabaseHandler.LOCATION_ID + " = ?", new String[] {
			String.valueOf(location.getId())
		});

		db.close();
	}

	public void deleteAll(Category dummy) { // Deleting all rows
		SQLiteDatabase db = this.getWritableDatabase();

		db.execSQL("DELETE FROM " + DatabaseHandler.TABLE_CATEGORY);

		db.close();
	}

	public void deleteAll(Location dummy) { // Deleting all rows
		SQLiteDatabase db = this.getWritableDatabase();

		db.execSQL("DELETE FROM " + DatabaseHandler.TABLE_LOCATION);

		db.close();
	}

	public Category getCategoryByName(String name) {
		SQLiteDatabase db = this.getReadableDatabase();

		String sql = "SELECT * FROM " + DatabaseHandler.TABLE_CATEGORY + " WHERE " + DatabaseHandler.CATEGORY_NAME + " = ?";

		String[] selectionArgs = {
			name,
		};

		Cursor cursor = db.rawQuery(sql, selectionArgs);

		if(!cursor.moveToFirst()) {
			db.close();
			return null;
		}

		Category category = new Category();

		category.fill_from_cursor(cursor);

		db.close();

		return category;
	}

	public Location getLocationByName(String name) {
		SQLiteDatabase db = this.getReadableDatabase();

		String sql = "SELECT * FROM " + DatabaseHandler.TABLE_LOCATION + " WHERE " + DatabaseHandler.LOCATION_NAME + " = ?";

		String[] selectionArgs = {
			name,
		};

		Cursor cursor = db.rawQuery(sql, selectionArgs);

		if(!cursor.moveToFirst()) {
			db.close();
			return null;
		}

		Location location = new Location(cursor);

		db.close();

		return location;
	}

	public Integer countLocationsByCategoryId(int category_id) {
		SQLiteDatabase db = this.getReadableDatabase();

		String sql = "SELECT COUNT(*) FROM " + DatabaseHandler.TABLE_LOCATION + " WHERE " + DatabaseHandler.LOCATION_CATEGORY_ID + " = ?";

		String[] selectionArgs = {
			String.valueOf(category_id),
		};

		Cursor cursor = db.rawQuery(sql, selectionArgs);

		if(!cursor.moveToFirst()) {
			db.close();
			return null;
		}

		int count = cursor.getInt(0);

		db.close();

		return count;
	}

	/*
	 * public Category getCategory(int id) { SQLiteDatabase db = this.getReadableDatabase();
	 * 
	 * // Cursor cursor = db.query(TABLE_CONTACTS, new String[] { // KEY_ID, // KEY_NAME, // KEY_PH_NO // }, KEY_ID + "=?", new String[] { // String.valueOf(id)
	 * // }, null, null, null, null);
	 * 
	 * String sql = "SELECT * FROM category";
	 * 
	 * Cursor cursor = db.rawQuery(sql, selectionArgs);
	 * 
	 * if(cursor != null) { cursor.moveToFirst(); }
	 * 
	 * int idc = Integer.parseInt(cursor.getString(0)); String name = cursor.getString(1); String phone = cursor.getString(2);
	 * 
	 * ContactItem ci = new ContactItem(); ci.setId(idc); ci.setName(name); ci.setPhone(phone); // return contact
	 * 
	 * return ci; }
	 */

	// public void dropAll() {
	// SQLiteDatabase db = this.getWritableDatabase(); // needs to close the
	//
	// // remember in reverse order from creation
	// db.execSQL("DROP TABLE IF EXISTS " + DatabaseHandler.TABLE_LOCATION);
	// db.execSQL("DROP TABLE IF EXISTS " + DatabaseHandler.TABLE_CATEGORY);
	// }
}
