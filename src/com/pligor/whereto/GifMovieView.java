package com.pligor.whereto;

import java.io.InputStream;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.os.SystemClock;
import android.view.View;

public class GifMovieView extends View {

	private Movie movie;
	private long movieStart = 0;

	public GifMovieView(Context context, InputStream newStream) {
		super(context);

		this.movie = Movie.decodeStream(newStream);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.TRANSPARENT);
		super.onDraw(canvas);

		final long now = SystemClock.uptimeMillis();

		if(this.movieStart == 0) {
			this.movieStart = now;
		}

		final int relTime = (int) ((now - movieStart) % this.movie.duration());
		this.movie.setTime(relTime);

		this.movie.draw(canvas, 0, 0);

		this.invalidate();
	}

}
