package com.pligor.whereto;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

/**
 * BE AWARE: An Overlay need at least one item before adding it to map view !!!!
 * 
 * @author pligor
 * 
 */
public class MyItemizedOverlay extends ItemizedOverlay implements OnClickListener {

	// protected List<OverlayItem> items = new ArrayList<OverlayItem>();
	protected Map<Integer, Location> items = new HashMap<Integer, Location>();
	protected Context context;

	protected MapView mapView;

	private AlertDialog removeLocationDialog;

	private int indexToBeDeleted;

	public MyItemizedOverlay(Drawable defaultMarker, Context context, MapView map_view) {
		// Adjusts a drawable's bounds so that (0,0) is a pixel in the center of the bottom of the drawable.
		super(ItemizedOverlay.boundCenterBottom(defaultMarker));
		this.context = context;
		this.mapView = map_view;
		this.removeLocationDialog = this.initDialog();
	}

	public void addItem(Location location) {
		// this.items.add(overlayItem);
		this.items.put(location.getId(), location);

		this.populate(); // this is crucial to render items
		this.mapView.invalidate();
	}

	public void removeItem(Location location) {
		// this.items.remove(overlayItem);
		this.items.remove(location.getId());

		this.populate(); // this is crucial to render items
		this.mapView.invalidate();
	}

	public void clearItems() {
		this.items.clear();

		this.populate(); // this is crucial to render items
		this.mapView.invalidate();
	}

	@Override
	protected OverlayItem createItem(int i) {
		Log.i("pl", "create item: " + i);
		return this.getItemByIndex(i);
	}

	@Override
	public int size() {
		return this.items.size();
	}

	protected boolean onTapParent(int index) {
		return super.onTap(index);
	}

	@Override
	protected boolean onTap(int index) {
		boolean tapped = super.onTap(index);

		this.indexToBeDeleted = index;
		Log.i("pl", "item id to be del: " + this.indexToBeDeleted);

		this.removeLocationDialog.show();

		return tapped;
	}

	private Location getItemByIndex(int index) {
		Object[] keyObjs = this.items.keySet().toArray();
		Log.i("pl", "index is: " + index);
		Integer key = (Integer) keyObjs[index];
		Log.i("pl", "key is: " + key);
		return this.items.get(key);
	}

	private AlertDialog initDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
		builder.setTitle(R.string.remove_dialog_title);
		String message = this.context.getResources().getString(R.string.remove_dialog_message);
		builder.setMessage(message);
		builder.setPositiveButton(R.string.remove_dialog_positive_button, this);
		builder.setNegativeButton(R.string.remove_dialog_negative_button, null);
		return builder.create();
	}

	public void onClick(DialogInterface dialog, int which) {
		if(dialog == this.removeLocationDialog) {
			if(which == AlertDialog.BUTTON_POSITIVE) {
				// OverlayItem item = this.items.get(this.indexToBeDeleted);
				// Location item = this.items.get(this.indexToBeDeleted);
				Location location = this.getItemByIndex(this.indexToBeDeleted);
				int location_id = location.getId();
				Log.i("pl", "location id is: " + location_id);

				if(location_id <= 2) {
					Toast toast = Toast.makeText(this.context, "This location is reserved", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					return;
				}

				this.removeItem(location);

				DatabaseHandler dbHandler = new DatabaseHandler(this.context);
				dbHandler.delete(location);
			}
		}
	}
}
