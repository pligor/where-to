package com.pligor.whereto;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Category {
	private int id;
	private String name;
	private String icon;

	private int countLocations = 0;

	public int getCountLocations() {
		return this.countLocations;
	}

	public String toString() {
		return this.name;
	}

	public List<Location> getLocations(DatabaseHandler dbHandler) {
		List<Location> locations = new ArrayList<Location>();

		SQLiteDatabase db = dbHandler.getReadableDatabase();

		String sql = "SELECT * FROM " + DatabaseHandler.TABLE_LOCATION + " WHERE " + DatabaseHandler.LOCATION_CATEGORY_ID + " = ?";

		String[] selectionArgs = {
			String.valueOf(this.id),
		};

		Cursor cursor = db.rawQuery(sql, selectionArgs);

		// Cursor cursor = db.query(DatabaseHandler.TABLE_LOCATION, new String[] { // it is discouraged to enter null in here, better specify each colum
		// seperately
		// DatabaseHandler.LOCATION_ID,
		// DatabaseHandler.LOCATION_NAME,
		// DatabaseHandler.LOCATION_LAT,
		// DatabaseHandler.LOCATION_LON,
		// }, DatabaseHandler.LOCATION_CATEGORY_ID + " = ?", new String[] {
		// String.valueOf(this.id)
		// }, null, null, null);

		if(cursor.moveToFirst()) {
			do { // looping through all rows and adding to list
				Location location = new Location(cursor);
				locations.add(location); // Adding contact to list
			}
			while (cursor.moveToNext());
		}

		db.close();

		this.countLocations = locations.size();

		return locations;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	// TODO den exoume lavei ypopsi tin periptosi opou apo ta N items pame sta miden. Ekei tha prepei na diagrapsoume olokliro to overlay!
	public void renderLocations(MyItemizedOverlay overlay, DatabaseHandler dbHandler) {
		overlay.clearItems();

		List<Location> locations = this.getLocations(dbHandler);

		for (Location location : locations) {
			// GeoPoint point = location.getGeoPoint();
			// OverlayItem overlayItem = new OverlayItem(point, location.getName(), location.getName());

			// overlay.addItem(overlayItem);
			// overlay.addItem(location_id, overlayItem);
			overlay.addItem(location);
		}
	}

	// TODO is this necessary??
	public int getDrawableId() {
		if(this.icon.contentEquals("parking")) {
			return R.drawable.parking;
		}
		else if(this.icon.contentEquals("gas_station")) {
			return R.drawable.gas_station;
		}
		return 0;
	}

	// public int getDrawableId() {
	// if(this.icon.contentEquals("parking")) {
	// // return getResources
	// }

	public void fill_from_cursor(Cursor cursor) {
		this.setId(Integer.parseInt(cursor.getString(0))); // TODO to getInt den mas kanei th douleia??
		this.setName(cursor.getString(1));
		this.setIcon(cursor.getString(2));
	}
}
