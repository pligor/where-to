package com.pligor.whereto;

import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

/**
 * TODO BE AWARE: An Overlay need at least one item before adding it to map view !!!!
 * 
 * @author pligor
 * 
 */
public class CurItemizedOverlay extends MyItemizedOverlay {

	public CurItemizedOverlay(Drawable defaultMarker, Context context, MapView map_view) {
		super(defaultMarker, context, map_view);
	}

	EditText nameEditText;
	String locationName;

	Spinner categorySpinner;
	Category selectedCategory;

	Button saveButton;

	@Override
	protected boolean onTap(int index) {
		Boolean tapped = super.onTapParent(index);

		final OverlayItem item = this.items.get(index);

		// Toast.makeText(this.context, "testing tap", Toast.LENGTH_LONG).show();
		final Dialog dialog = new Dialog(this.context);
		dialog.setContentView(R.layout.tapdialog);
		dialog.setTitle("Please fill the information");
		dialog.setCancelable(true);

		this.nameEditText = (EditText) dialog.findViewById(R.id.nameEditText);

		this.categorySpinner = (Spinner) dialog.findViewById(R.id.categorySpinner);
		ArrayAdapter<Category> spinnerArrayAdapter = new ArrayAdapter<Category>(this.context, android.R.layout.simple_spinner_dropdown_item,
				this.get_all_categories());
		categorySpinner.setAdapter(spinnerArrayAdapter);

		this.saveButton = (Button) dialog.findViewById(R.id.saveButton);

		this.saveButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// check for duplicate location

				GeoPoint geoPoint = item.getPoint();

				Boolean isLocationUnique = CurItemizedOverlay.this.isLocationUnique(geoPoint);

				try {

					if(!isLocationUnique) {
						Toast.makeText(CurItemizedOverlay.this.context, "Duplicate Locations are not allowed", Toast.LENGTH_LONG).show();
						throw new Exception("duplicate location");
					}

					CurItemizedOverlay.this.collect_form_inputs();

					String name = CurItemizedOverlay.this.locationName;
					if(name.contentEquals("")) {
						Toast.makeText(CurItemizedOverlay.this.context, "Location name cannot be empty!", Toast.LENGTH_LONG).show();
						return;
					}

					// save to database
					DatabaseHandler dbHandler = new DatabaseHandler(CurItemizedOverlay.this.context);
					Location location = new Location(geoPoint, name, name);
					location.setName(name);
					// location.setLat(GPSReceiver.convDegreesToDouble(geoPoint.getLatitudeE6()));
					// location.setLon(GPSReceiver.convDegreesToDouble(geoPoint.getLongitudeE6()));

					Category cat = CurItemizedOverlay.this.selectedCategory;

					dbHandler.insert(location, cat); // TODO check if here we get the new id to the location model

					// add overlay item to appropriate overlay
					// OverlayItem overlayItem = new OverlayItem(location.getGeoPoint(), location.getName(), location.getName());

					MainActivity mainActivity = (MainActivity) CurItemizedOverlay.this.context;
					int overlay_index = mainActivity.category_overlay.get(cat.getId()); // get overlay index from category id
					MyItemizedOverlay overlay = (MyItemizedOverlay) mainActivity.mapOverlays.get(overlay_index);
					// overlay.addItem(location.getId(), overlayItem);
					// overlay.addItem(overlayItem);
					overlay.addItem(location);

					// remove this overlay
					mainActivity.mapOverlays.remove(CurItemizedOverlay.this);
				}
				catch (Exception e) {
					Log.i("pl", e.getMessage());
				}

				dialog.dismiss();
			}
		});

		dialog.show();

		return tapped;
	}

	private void collect_form_inputs() {
		// get name
		this.locationName = this.nameEditText.getText().toString();
		this.selectedCategory = (Category) this.categorySpinner.getSelectedItem();
	}

	private String[] get_all_category_names() {
		// List<String> names = new ArrayList<String>();

		DatabaseHandler dbHandler = new DatabaseHandler(this.context);

		List<Category> categories = dbHandler.getAll(new Category());

		String[] names = new String[categories.size()];

		int i = 0;
		for (Category category : categories) {
			names[i++] = category.getName();
		}

		// return (String[]) names.toArray();
		return names;
	}

	private Category[] get_all_categories() {
		DatabaseHandler dbHandler = new DatabaseHandler(this.context);

		List<Category> categoryList = dbHandler.getAll(new Category());

		Category[] categories = new Category[categoryList.size()];

		int i = 0;
		for (Category category : categoryList) {
			categories[i++] = category;
		}

		return categories;
	}

	private Boolean isLocationUnique(GeoPoint geoPoint) {
		DatabaseHandler dbHandler = new DatabaseHandler(this.context);

		double lat = GPSReceiver.convDegreesToDouble(geoPoint.getLatitudeE6());
		double lon = GPSReceiver.convDegreesToDouble(geoPoint.getLongitudeE6());

		SQLiteDatabase db = dbHandler.getReadableDatabase();

		String sql = "SELECT COUNT(*) FROM location WHERE lat = ? AND lon = ?";
		String[] selectionArgs = {
				String.valueOf(lat),
				String.valueOf(lon),
		};
		Cursor cursor = db.rawQuery(sql, selectionArgs);

		cursor.moveToFirst();
		int count = cursor.getInt(0);
		db.close();

		if(count == 0) {
			return true;
		}
		else if(count == 1) {
			Log.i("pl", "duplicate location found!");
			return false;
		}

		return null;
	}
}
