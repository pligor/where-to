package com.pligor.whereto;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

/**
 * BE AWARE: An Overlay need at least one item before adding it to map view !!!!
 * 
 * @author pligor
 * 
 */
public class MainActivity extends MapActivity implements OnClickListener {
	// public class MyMapActivity extends Activity {

	public MapView map_view;
	public List<Overlay> mapOverlays;
	private GPSReceiver gpsReceiver;

	private CurItemizedOverlay curOverlay = null;

	private DatabaseHandler dbHandler;

	public Map<Integer, Integer> category_overlay = new HashMap<Integer, Integer>();

	InputStream stream = null;
	GifMovieView gifView;
	RelativeLayout mapLayout;
	ImageButton spotButton;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.main);

		this.dbHandler = new DatabaseHandler(this);

		this.map_view = (MapView) this.findViewById(R.id.map_view);
		this.map_view.setBuiltInZoomControls(true);

		this.mapOverlays = this.map_view.getOverlays();

		this.mapLayout = (RelativeLayout) this.findViewById(R.id.mapLayout);

		//this.initGifView();
		spotButton = (ImageButton) findViewById(R.id.spotButton);
		spotButton.setOnClickListener(this);

		Toast toast = Toast.makeText(this, "In the rare occasion it crashes just restart. Everything is always saved!", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.BOTTOM, 0, 0);
		toast.show();
	}
/*
	private void initGifView() {
		try {
			this.stream = this.getAssets().open("clickhere.gif");
			this.gifView = new GifMovieView(this, this.stream);
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		this.mapLayout.addView(this.gifView, 70, 70);

		this.gifView.setOnClickListener(this);
	}
//*/
	public void onClick(View v) {
		//if(v.getId() == this.gifView.getId()) {
		if(v.getId() == spotButton.getId()) {
			this.find_current_location(v);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		this.gpsReceiver = new GPSReceiver(this);
		this.gpsReceiver.startLocationManager();

		this.addItemizedOverlaysFromCategories();
	}

	@Override
	protected void onPause() {
		super.onPause();

		this.gpsReceiver.stopLocationManager();
		this.gpsReceiver = null;

		this.mapOverlays.clear();
	}

	private void addItemizedOverlaysFromCategories() {
		List<Category> categories = this.dbHandler.getAll(new Category());

		// Log.i("pl", "categories: " + categories.size());

		Drawable drawable = this.getResources().getDrawable(R.drawable.ic_launcher);

		MyItemizedOverlay overlay;

		for (Category category : categories) {
			try {
				drawable = this.getResources().getDrawable(category.getDrawableId());
			}
			catch (NotFoundException e) {
				e.printStackTrace();
				Log.i("pl", "drawable was not found");
			}

			List<Location> locations = category.getLocations(this.dbHandler);
			// Log.i("pl", "locations: " + locations.size());

			if(category.getCountLocations() > 0) {
				overlay = new MyItemizedOverlay(drawable, this, this.map_view);

				this.mapOverlays.add(overlay);

				int overlay_id = this.mapOverlays.size() - 1;
				this.category_overlay.put(category.getId(), overlay_id); // koumponoume category_id me overlay_id (java sucks!)

				category.renderLocations(overlay, this.dbHandler);
			}

			// Log.i("pl", "added itemized overlay with drawable: " + icon);
		}
	}

	public void find_current_location(View v) {
		int lat = this.gpsReceiver.getCurrentLatitude();
		int lon = this.gpsReceiver.getCurrentLongtitude();
		if(lat == 0 || lon == 0) {
			Toast toast = Toast.makeText(this, "You are not spotted yet. Please wait or restart the program and move outdoors for better GPS signal",
					Toast.LENGTH_LONG);
			toast.show();
			return;
		}

		// double latitude = 38.1233825 + Math.random() / 100;
		// double longitude = 23.7580785 + Math.random() / 100;
		// int lat = GPSReceiver.convDegreesToInt(latitude);
		// int lon = GPSReceiver.convDegreesToInt(longitude);

		Log.i("pl", "Found you at lat: " + lat + " and lon: " + lon);

		GeoPoint point = new GeoPoint(lat, lon);
		// OverlayItem item = new OverlayItem(point, "current", "current");
		Location location = new Location(point, "current", "current");

		// create new overlay on the fly

		this.mapOverlays.remove(this.curOverlay); // remove if it exists already

		this.curOverlay = new CurItemizedOverlay(this.getResources().getDrawable(R.drawable.current), this, this.map_view);

		// this.curOverlay.addItem(item);
		// this.curOverlay.addItem(0, item); // zero(0) id because we just don't care for this temporary item
		this.curOverlay.addItem(location);

		this.mapOverlays.add(this.curOverlay);

		this.map_view.getController().animateTo(point);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	public void export_demo(View view) {
		String jsonString = this.dbHandler.export_to_json();
		Log.i("pl", jsonString);
	}

	public void import_demo(View view) {
		String input = "[\n" + "		 {\n" + "		     \"Gas Stations\": {\n" + "		         \"locations\": [\n" + "		             {\n"
				+ "		                 \"BP\": {\n" + "		                     \"lon\": 23.759,\n" + "		                     \"lat\": 38.1242\n"
				+ "		                 }\n" + "		             },\n" + "		             {\n" + "		                 \"Shell\": {\n"
				+ "		                     \"lon\": 23.7557,\n" + "		                     \"lat\": 38.1214\n" + "		                 }\n" + "		             }\n"
				+ "		         ],\n" + "		         \"icon\": \"gas_station\"\n" + "		     }\n" + "		 },\n" + "		 {\n" + "		     \"Parking Day time only\": {\n"
				+ "		         \"locations\": [\n" + "		             {\n" + "		                 \"Safe and Sound\": {\n"
				+ "		                     \"lon\": 23.7605,\n" + "		                     \"lat\": 38.121\n" + "		                 }\n" + "		             },\n"
				+ "		             {\n" + "		                 \"Best parking\": {\n" + "		                     \"lon\": 10,\n"
				+ "		                     \"lat\": 11\n" + "		                 }\n" + "		             }\n" + "		         ],\n"
				+ "		         \"icon\": \"parking\"\n" + "		     }\n" + "		 }\n" + " ]";

		this.dbHandler.import_from_json(input);

		Log.i("pl", "json imported ok");
	}
}
