package com.pligor.whereto;

import android.database.Cursor;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class Location extends OverlayItem {
	// public Location() {
	// super(null, null, null);
	// }

	public Location(Cursor cursor) {
		super(Location.getPointFromCursor(cursor), Location.getNameFromCursor(cursor), Location.getNameFromCursor(cursor));
		this.fill_from_cursor(cursor);
	}

	public Location(GeoPoint point, String title, String snippet) {
		super(point, title, snippet);
		this.setCoordsFromGeoPoint();
	}

	private int id;
	private String name;
	private double lat;
	private double lon;
	private int category_id;

	public GeoPoint getGeoPoint() {
		return new GeoPoint(GPSReceiver.convDegreesToInt(this.lat), GPSReceiver.convDegreesToInt(this.lon));
		// return this.getPoint();
	}

	public void setCoordsFromGeoPoint() {
		this.lat = GPSReceiver.convDegreesToDouble(this.getPoint().getLatitudeE6());
		this.lon = GPSReceiver.convDegreesToDouble(this.getPoint().getLongitudeE6());
	}

	// TODO return the object Category better
	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLat() {
		return lat;
	}

	// public void setLat(double lat) {
	// this.lat = lat;
	// }

	public double getLon() {
		return lon;
	}

	// public void setLon(double lon) {
	// this.lon = lon;
	// }

	public static String getNameFromCursor(Cursor cursor) {
		return cursor.getString(1);
	}

	public static GeoPoint getPointFromCursor(Cursor cursor) {
		double lat = cursor.getDouble(2);
		double lon = cursor.getDouble(3);
		return GPSReceiver.getGeoPointFromLatAndLon(lat, lon);
	}

	private void fill_from_cursor(Cursor cursor) {
		this.id = cursor.getInt(0);
		this.name = cursor.getString(1);
		this.lat = cursor.getDouble(2);
		this.lon = cursor.getDouble(3);
		this.category_id = cursor.getInt(4);

		// this.setId(Integer.parseInt(cursor.getString(0)));
		// this.setName(cursor.getString(1));
		// this.setLat(Double.parseDouble(cursor.getString(2)));
		// this.setLon(Double.parseDouble(cursor.getString(3)));
		// this.setCategory_id(Integer.parseInt(cursor.getString(4)));
	}
}
