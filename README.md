User Locations
Save to database: Name, Lat, Long

user1 listens and user2 listens as well

user can share / broadcast ALL favorite places to ONE nearby bluetooth device
OR
user can request ALL favorites from a specific nearby bluetooth

For example
user1 sends. user2 gets a dialog to accept or reject and the transfer occurs
OR
user1 requests. users2 gets another dialog to accept or reject and the transfer occurs

After transfer, the database is updated accordingly
